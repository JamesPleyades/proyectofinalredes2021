# ProyectoFinalRedes2021

Repositorio para entregar el proyecto final

Roberto Andrés Ledesma Granados		414070215
Rodrigo Alvar Reyna Trejo		312196785
Martínez López Jaime Alberto		309256753

Archivos
- chatbot.py 
- ex2.py
- bot.sh
- redesapi.service
- redeschatbot.service

Licencias

- API:
	psutil: Licencia BSD
	speedtest-cli: Apache Software License
	fastapi: MIT license
	uvicorn: BSD-3-Clause License
	typing: Python Software Foundation License (PSF)

- Chatbot:
	telegram: BSL-1.0 License


Descripción

Al inicio se propuso la idea de hacer el bot en python,apoyandonos de los módulos requests y psutil, requests para mandar las peticiones http directamente a la api de telegram y psutil para obtener la información del servidor, esto se encuentra en el archivo prueba2-bot.py.

Sin embargo, se optó por hacer mejoras en general al proyecto, en total se usan 4 archivos principales, chatbot.py, ex2.py, bot.sh y redesapi.service.

En ex2.py se hace uso de los módulos Optional (para verificar el tipo del parámetro), fastapi (para crear el web framework), psutil (para verifiar los datos de memoria RAM y estado del CPU), speedtest (para obtener la velocidad de carga y descarga), uvicorn (para levantar el servidor) y por último se encripta el mensaje a enviar mediante el cifrado simétrico de fernet.

En chatbot.py se usan los módulos subproces, requests (para hacer la conexión con el servidor de amazon y para usar el método get, conectarnos a 'https://sandrigite.tk/status', cargar el mensaje y que nuestro bot lo envíe a telegram), json (para darle formato a la información solicitada), decimal (para pasar de bytes a Mb) y telegram (para poder crear un bot, comunicarnos con la api, enviar la información solicitada y actualizar).

En bot.sh, mediante privilegios de sudo se llama a uvicorn, se incluye el certificado, su llave, el host: 0.0.0.0 y el puerto: 4.4.3

Se inicia una aplicación y disitintos métodos get (algunos de prueba para verificar que el servicio funcione) para que con el comando "/status" a través de telegram se nos envíe la información solicitada.

Después el archivo redesapi.service manda a llamar a bot.sh, el cual a través de uvicorn (con host 0.0.0.0 y puerto 443) manda a llamar a ex2.py y a los parámetros necesarios para poder ser visto en la red. 

Finalmente se creó el correspondiente servicio para el chatbot para poder hacer las peticiones sin tener que conectarnos vía ssh a la instancia.
