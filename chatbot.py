import subprocess
import requests
import json
import decimal
from telegram import *
from telegram.ext import *

def convert(bytes):
  return round(bytes * 9.537e-7, 3)

def server_info(update:Update,context:CallbackContext):
  print(update.message.text)
  bot.send_message(
    chat_id = update.effective_chat.id,
    text = "Estamos obteniendo tu información...")
  response = requests.get('https://ec2-18-204-200-222.compute-1.amazonaws.com/status',verify=False)
  msg = json.loads(response.text)
  mem = msg['ram_total'] - msg['ram_disponible']
  answer = "Memoria: " + str(convert(mem)) + " Mb\n"
  answer = answer + "CPU: " + str(msg['cpu']) + " %\n"
  answer = answer + "Conexión: \nVelocidad de Bajada " + str(convert(msg['velodicad_bajada'])) + " Mbps \n"
  answer = answer + "Velocidad de Subida " + str(convert(msg['velocidad_subida'])) + " Mbps \n" 
  bot.send_message(
    chat_id = update.effective_chat.id,
    text = answer )

def server_info2(context):
  #print(update.message.text)
  bot.send_message(
    chat_id = context.job.context,
    text = "Estamos obteniendo tu información...")
  response = requests.get('https://ec2-18-204-200-222.compute-1.amazonaws.com/status',verify=False)
  msg = json.loads(response.text)
  mem = msg['ram_total'] - msg['ram_disponible']
  answer = "Memoria: " + str(convert(mem)) + " Mb\n"
  answer = answer + "CPU: " + str(msg['cpu']) + " %\n"
  answer = answer + "Conexión: \nVelocidad de Bajada " + str(convert(msg['velodicad_bajada'])) + " Mbps \n"
  answer = answer + "Velocidad de Subida " + str(convert(msg['velocidad_subida'])) + " Mbps \n" 
  bot.send_message(
    chat_id = context.job.context,
    text = answer )
def status_report(update:Update,context:CallbackContext):
	cid=update.effective_chat.id
	bot.send_message(chat_id=cid,
	text='Iniciando actualizacion periodica')
	context.job_queue.run_repeating(callback=server_info2,
	interval=5,context=cid)

def is_awake(update:Update,context:CallbackContext):
  bot.send_message(chat_id=update.effective_chat.id,text='copiado')


def asul(update:Update,context:CallbackContext):
    chat_id = update.effective_chat.id
    print(chat_id)
    response = requests.get(
    'https://sandrigite.tk/asul',
    verify=False)
    msg = json.loads(response.text)
    bot.send_message(
    chat_id = update.effective_chat.id,
    text =msg )

def asul2(context):
    response = requests.get(
    'https://sandrigite.tk/asul',
    verify=False)
    msg = json.loads(response.text)
    #print(context.kargs)
    print(context.job.context)
    bot.send_message(
    chat_id=context.job.context,
    text =msg )



def asul_rep(update:Update,context:CallbackContext):
	cid=update.effective_chat.id
	bot.send_message(chat_id=cid,
	text='Iniciando actualizacion periodica')
	context.job_queue.run_repeating(callback=asul2,
	interval=2,context=cid)

def stop_asul_rep(update:Update,context:CallbackContext):
	print('no')

#hay que sacar la llave de botfather
token_key = "1562824341:AAHNafzbtOJJnJ57wlaiQNWqPBNQn8OeHq0"

bot = Bot(token_key)
#print(bot.get_me())

updater = Updater(token_key, use_context=True)
dispatcher = updater.dispatcher
#jq=updater.job_queue
start_value = CommandHandler('status', server_info)
dispatcher.add_handler(start_value)
dispatcher.add_handler(CommandHandler('copia',is_awake))
dispatcher.add_handler(CommandHandler('asul', asul))
dispatcher.add_handler(CommandHandler('asul_rep',
asul_rep,pass_job_queue=True))
dispatcher.add_handler(CommandHandler('status_report',
status_report,pass_job_queue=True))

updater.start_polling()
