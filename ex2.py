#from cryptography.fernet import Fernet
from typing import Optional
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import psutil as ps
import speedtest as spd
#from app.api import viz
import uvicorn
app = FastAPI()


ps.cpu_percent() #pimera lectura para poder dar el estado del cpu
app.add_middleware(
CORSMiddleware,
allow_origins=['*'],
allow_credentials=True,
allow_methods=['*'],
allow_headers=['*'],

)


def encrypt(message):
    key = open("secret.key", "rb").read()
    f = Fernet(key)
    encrypted_message = f.encrypt(message.encode())
    return encrypted_message

@app.get("/api")
def read_root():
    return {"Hello": "World"}

@app.get("/asul")
def read_root():
    return {"ASUL":"Hola Michael"}

@app.get("/status")
def read_root():
	mem=ps.virtual_memory()
	vel=spd.Speedtest()
	ram_t=mem.total
	ram_d=mem.available
	return {
	"cpu":ps.cpu_percent(),
	"ram_total":ram_t,
	"ram_disponible":ram_d,
	"velocidad_subida":vel.upload(),
	"velodicad_bajada":vel.download()
	}

@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


#print('cpu: %')
#print('RAM usada: ', (mem.total-mem.available)/1024**3,' GB')
#print('Velocidad de Bajada: ',vel.download())
#print('Velocidad de Subida: ',vel.upload())
#print('Ping :', vel.ping())
